# Sistem Operasi F - Laporan Resmi Shift 2
Kelas Sistem Operasi F - Kelompok F02

## Anggota Kelompok :
- Salsabila Fatma Aripa (5025211057)
- Arfi Raushani Fikra (5025211084)
- Ahmad Fauzan Alghifari (5025211091)

### Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun.

**a.** Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

**b.** Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

**c.** Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

**d.** Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

**Penyelesaian**

**a.** Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

Pertama, lakukan download dengan url yang sudah diberikan. Proses tersebut akan dilakukan dengan fungsi berikut
```c
download(filename, "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");

//definisi fungsi
void download(char file[], char url[]) {
    int status;
    pid_t pid = fork();
    if(pid==0){
        char *argv[] = {"wget",
                        "-O",  // output file
                        file,  // specified output file name
                        url,   // URL of the file to download
                        NULL};

        execvp("wget", argv);
    } 
    else if (pid < 0){
        printf("fork error");
        exit(1);
    }
    else {
        wait(&status);
    }
}
```

Setelah itu, Grape-kun perlu melakukan unzip pada folder tersebut. Proses tersebut dapat dilakukan dengan fungsi berikut
```c
  unzip(filename);

  //definisi fungsi
  void unzip(char file[]){
    pid_t pid = fork();

    if(pid == 0){
        char *arg[] = {"unzip", "-q", file, "-d", "downloaded", NULL};
        execvp("unzip", arg);
    } else if (pid < 0){
        printf("fork error");
        exit(1);
    }
}
```

![image](/uploads/38a0830f35e15dddc6aaed898455cd19/image.png)

**b.** Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

Sebelum masuk ke dalam fungsi utama penyelesaian soal b, ada beberapa fungsi tambahan yang perlu diimplementasikan.

Fungsi pertama adalah getFileNumAtDir(). Fungsi ini akan menghitung jumlah file di dalam directory.
```c
int getFileNumAtDir(char path[]){
    DIR *dp = opendir(path);
    struct dirent *ep;
    int count = 0;
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
           if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0){
                continue;
           }
           count++;
      }

      (void) closedir (dp);
    } else {
        perror ("Couldn't open the directory");
        return - 1;
    }

    return count;
}
```

Fungsi Kedua adalah getRandNumber() fungsi ini akan mencari bilangan acak dari 1 sampai jumlah file dalam directory.
```c
int getRandNumber(char folderName[]){
    srand(time(NULL));
    return rand()%getFileNumAtDir(folderName);
}
```

Kemudian untuk fungsi utamanya adalah sebagai berikut. Fungsi ini pertama akan mengambil nilai randomnumber, kemudian akan melakukan iterasi pada directory, kemudian mencari file ke-nilai random tersebut. setelah itu, file tersebut akan dikembalikan sebagai nilai balik dalam bentuk string.
```c
char* chooseRandom(char foldername[]){
    int r = getRandNumber(foldername);
    int count = 0;
    DIR *dp = opendir(foldername);
    struct dirent *ep;

    char *returnVal = NULL;
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
           if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0){
                continue;
           }

           if(count == r){
                returnVal = (char*)malloc(strlen(ep->d_name)+2);
                returnVal = (char*)ep->d_name;
                break;
           }
           count++;
      }

      (void) closedir (dp);
    } else {
        perror ("Couldn't open the directory");
        return NULL;
    }

    return returnVal;
}
```

kemudian, untuk menyelesaikan soal ini, cukup melakukan pemanggilan fungsi di atas.
```c
  printf("%s", chooseRandom("./downloaded"));
```
![image](/uploads/56f1f27762426f9cdc9ec5c894a8e21e/image.png)

**c.** Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

Pertama, implementasi fungsi berikut untuk membuat direktori baru.
```c
void createDir(char *foldername){
    pid_t pid = fork();

    if(pid==0){
        char *arg[] = {"mkdir", foldername, NULL};
        execvp("/bin/mkdir", arg);
        exit(0); 
    }
    else{
        wait(NULL); 
    }
}
```

Kedua, untuk membuat direktori HewanDarat, HewanAmphibi, dan HewanAir, dibuat fungsi berikut untuk melakukan hal tersebut
```c
void createDirAll(){
    createDir("HewanDarat");
    createDir("HewanAir");
    createDir("HewanAmphibi");
}
```

Kemudian, karena sekarang sudah ada direktori yang diinginkan, tinggal melakukan pemindahan dari folder ```dowloaded``` ke folder-folder tersebut. dimana ini akan seperti directory listing, dan ketika menemukan suatu file, maka akan langsung melakukan proses pemindahan ke direktori yang sesuai.

```c
void mainMove(char *det){
    char path[] = "downloaded";
    

    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, det)){
                char destPath[256];

                if(strstr(ep->d_name, "air")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Air");
                    moveFile(ep->d_name, path, destPath);
                }

                else if(strstr(ep->d_name, "darat")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Darat");
                    moveFile(ep->d_name, path, destPath);
                }

                else if(strstr(ep->d_name, "amphibi")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Amphibi");
                    moveFile(ep->d_name, path, destPath);
                }
                
            }
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}
```

dimana definisi untuk fungsi moveFile() adalah sebagai berikut
```c
void moveFile(char *filename, char *src, char *dest){
    pid_t pid = fork();
    if(pid==0){
        char srcfix[256];
        snprintf(srcfix, sizeof(srcfix), "%s/%s", src, filename);  

        char destfix[256];
        snprintf(destfix, sizeof(destfix), "%s/%s", dest, filename); 

        char *mv_arg[] = {"mv", srcfix, destfix, NULL};
        execvp("/bin/mv", mv_arg);

        exit(0); 
    }
    else{
        wait(NULL);
    }
}
```

Cara penggunaan dari fungsi mainMove adalah sebagai berikut
```c
mainMove("air");  
mainMove("darat");
mainMove("amphibi");
```
![image](/uploads/7a99f9127e7f6d6590c2dd2df99950ca/image.png)

**d.** Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Dari pernyataan soal, diminta untuk melakukan zip dari direktori direktori yang sudah dibuat. Kemudian hapus direktori tersebut untuk menghemat penyimpanan.

Pertama, akan dilakukan zipping menggunakan exec. Setelah proses zipping selesai, akan dipanggil fungsi removeMe() untuk menghapus direktori yang baru saja di-zip.
```c
void zipDirAndContents(char folderName[]){
    int status;
    pid_t pid = fork();
    
    char *zipfile = NULL;
    zipfile = (char*)malloc(strlen(folderName)+5);
    strcpy(zipfile, folderName);
    strcat(zipfile, ".zip");
    if(pid == 0){
        char *zip_args[] = {"zip", "-r", zipfile, folderName, NULL};
        execvp("zip", zip_args);
    } else {
        wait(&status);
        removeMe(folderName);
    }
    free(zipfile);
}
```

Berikut merupakan fungsi removeMe()
```c
void removeMe(char *foldername) {
    int status;
    pid_t pid = fork();

    if(pid == 0){
        char *rm_args[] = {"rm", "-rf", foldername, NULL};
        execvp("rm", rm_args);
    } else {
        wait(&status);
    }
}
```
![image](/uploads/0d1bbe0eb19ba5b457b8652692a4fa09/image.png)

### Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi!

**Ketentuan**

**a.** Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp `[YYYY-MM-dd_HH:mm:ss]`.

**b.** Tiap-tiap folder lalu diisi dengan 15 gambar yang di-download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran `(t%1000)+50` piksel dimana `t` adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp `[YYYY-mm-dd_HH:mm:ss]`.

**c.** Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan `.zip`).

**d.** Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program `"killer"` yang siap di `run(executable)` untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

**e.** Buatlah program utama bisa dirun dalam dua mode, yaitu `MODE_A` dan `MODE_B`. untuk mengaktifkan `MODE_A`, program harus dijalankan dengan argumen `-a`. Untuk `MODE_B`, program harus dijalankan dengan argumen `-b`. Ketika dijalankan dalam `MODE_A`, program utama akan langsung menghentikan semua operasinya ketika program `killer` dijalankan. Untuk `MODE_B`, ketika program `killer` dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di-delete).

**Catatan :**

- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)


**Penyelesaian :**

Untuk menyelesaikan soal ini, dibuat sebuah fungsi bernama `helpSucipto()`. Fungsi ini nantinya akan dipanggil melalui fungsi `main`. Fungsi `helpSucipto()` ini berisikan beberapa fungsi yang diperlukan seperti fungsi `createFolder()` untuk membuat folder baru, fungsi `DownZip()` untuk mendownload gambar ke dalam folder dan zip folder tersebut. Dikarenakan proses berjalan secara `daemon`, maka dilakukan prosedur pembuatan `daemon` di dalam fungsi `main()` sebagai berikut :

Langkah pertama yaitu membuat sebuat `parent` process dengan melakukan `fork()`. Kemudian parent process dibunuh
```c
pid_t pid, sid;
char folName[80];
time_t tnow;

pid = fork();

if(pid < 0){
   exit(EXIT_FAILURE);
}

if(pid > 0){
   exit(EXIT_SUCCESS);
}
```
Pada kode di atas, juga terdapat deklarasi array bertipe `char` bernama `folName` untuk digunakan pada proses berikutnya. Adapun variabel `tnow` bertipe `time_t` digunakan untuk memperoleh waktu saat ini. Setelah melalukan tahapan `fork()`, berikutnya perlu dilakukan pengubahan mode file dengan bantuan `umask`. Untuk mendapatkan akses penuh dari file, maka `umask` diatur menjadi `umask(0)`.
```c
umask(0);
```

Agar `child` process tidak menjadi `orphan process`, maka proses `child` tersebut harus dibuatkan SID dengan menggunakan perintah `setsid`
```c
sid = setsid();
if(sid < 0){
   exit(EXIT_FAILURE);
}
```
Dikarenakan sebuah `daemon` tidak menggunakan terminal, maka descriptor standar `(STDIN, STDOUT, STDERR)` harus ditutup.
```c
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```

Untuk melacak jalannya proses penyelesaian soal ini, maka dibuatlah sebuah file bernama `prog_log` dengan baris kode sebagai berikut
```c
FILE *prog_log;

//membuka file log_prog.txt dengan mode write dan read
prog_log = fopen("log_prog.txt", "w+");
```

Program ini nantinya akan dijalankan dengan argumen tertentu, argumen ini bertujuan untuk menginisialisasi program `killer`. Maka dari itu, pada file `prog_log` akan dituliskan pesan yang menandakan adanya program `killer` tereksekusi. Implementasinya pada kode berikut
```c
fprintf(prog_log,"Program Assassin exists\n");
fflush(prog_log);
```

Setelah pesan dimasukkan dengan bantuan perintah `fflush()`, maka file `prog_log` ditutup dengan kode berikut
```c
fclose(prog_log);
```

Setelah daemon dibuat, maka dilakukan pemanggilan fungsi `helpSucipto()` untuk melakukan pembuatan folder setiap 30 detik dengan format nama tertentu

Pertama didapatkan terlebih dahulu `timestamp` pada saat tersebut dengan fungsi `time()` dan `localtime()` serta di-format mengikuti susunan `[YYYY-MM-dd_HH:mm:ss]` dengan bantuan fungsi `strftime()`. Format nama tersebut disimpan di array `folName`. Berikut kode-nya;
```c
time(&tnow);
strftime(folName, 80, "%Y-%m-%d_%X", localtime(&tnow));
```

Setelah nama folder berhasil didapatkan, maka proses berikutnya yaitu membuat folder dengan bantuan fungsi `createFolder()`
```c
void createFolder(char *folName)
{
  if(fork() == 0){
    char *argv[] = {"mkdir", "-p", folName, NULL};
    execv("/bin/mkdir", argv);
  }
}
```
Fungsi ini menggunakan `fork()` untuk membuat `child process` agar dapat melakukan eksekusi perintah `mkdir` yang disimpan dalam array `argv`. Argumen pada `argv` setelah itu dijalankan dengan menggunakan perintah `execv`. Nama folder didapat dari isi array `folName` sebelumnya.

Setelah pemanggilan fungsi `createFolder()`, tahap berikutnya yaitu melakukan proses `download` dan `zip` dengan menggunakan fungsi `DownZip()`.

Fungsi `DownZip()` ini melakukan `forking` juga dan memanggil dua fungsi lain untuk membantunya, yaitu fungsi `download()` dan fungsi `zip()`. Pada fungsi `DownZip()` ini juga dilakukan penulisan pesan ke dalam file `prog_log` untuk melacak proses download dan zip folder. Fungsi ini mengambil argumen `folName` untuk memberi kejelasan nama file tujuan download dan zip. Berikut implementasi kode-nya;
```c
void DownZip(char *folName)
{
   if(fork() == 0){
     fprintf(prog_log, "Created a folder named %s\n", folName);
     fflush(prog_log);

     // fungsi untuk melakukan download
     download(folName);

     //fungsi untuk melakukan zip
     zip(folName);
   }
}
```

Adapun fungsi `download()` akan melakukan proses download sebanyak 15 gambar dengan jeda tiap download adalah 5 detik. Untuk melakukan 15 kali download, maka digunakan `for-loop` dengan iterasi 15 kali dan untuk melakukan jeda antar-download, digunakan bantuan fungsi `sleep()` yang diisikan nilai 5 sebagai jeda 5 detik. 
```c
sleep(5); // dilakukan tiap 5 detik
```

Pada fungsi `download()` ini, diperlukan variabel untuk mencatat waktu dengan variabel bertipe `time_t` bernama `tnow`, lalu terdapat variabel bertipe `int` untuk iterasi dan menyimpan ukuran download gambar, selain itu, juga dibutuhkan 3 array tambahan yang digunakan untuk proses pembuatan nama file gambar hasil download bernama `pictName[]`, penampung sementara `timestamp` nama download bernama `buffer[]`, dan array untuk menampung link download bernama `link[]`.
```c
time_t tnow;
int i, size;
char pictName[201], buffer[200], link[200];
```

Seperti proses pembuatan nama folder, nama file gambar hasil download dibuat dengan bantuan fungsi `strftime()` lalu dipasangkan dengan bantuan fungsi `sprintf()`
```c
time(&tnow);
strftime(buffer, 200, "%Y-%m-%d_%X", localtime(&tnow));
sprintf(pictName, "%s/%s", folName, buffer); // nama file gambar yang di-download
```

Untuk mendapatkan ukuran gambar yang di-download dengan operasi `(t % 1000) + 50`, dimana `t` merupakan detik epoch UNIX, maka dilakukan sesuai kode berikut;
```c
size = (int)time(NULL); 		
size = (size % 1000) + 50;
``` 
Setelah itu, variabel `size` dipasangkan dengan isi dari variabel `link[]` seperti berikut
```c
sprintf(link, "https://picsum.photos/%d", size);
```

Proses download dilakukan dengan bantuan perintah `fork()` dan `execv` yang mengeksekusi argumen dari array `argv` yang berisi perintah `wget`. Berikut implemantasinya;
```c
if(fork() == 0){
    char *argv[] = {"wget", "-qO", pictName, link, NULL};
    execv("/usr/bin/wget", argv);
}
```

Setelah proses download setiap gambar selesai, kembali dituliskan ke dalam file `prog_log` sebuah pesan bahwa gambar telah berhasil di-download.
```c
fprintf(prog_log, "Picture %s successfully downloaded\n", pictName);
fflush(prog_log);
```
Berikut implementasi lengkap dari fungsi `download()`
```c
void download(char *folName)
{
  time_t tnow;
  int i, size;
  char pictName[201], buffer[200], link[200];

  for(i=0; i < 15; i++) // 15 gambar
  {
    time(&tnow);
    strftime(buffer, 200, "%Y-%m-%d_%X", localtime(&tnow));
    sprintf(pictName, "%s/%s", folName, buffer); // nama file gambar yang di-download

    // mendapatkan Epoch Unix dan mengatur size download 
    size = (int)time(NULL); 	
    size = (size % 1000) + 50;
    sprintf(link, "https://picsum.photos/%d", size);

    if(fork() == 0)
    {
      char *argv[] = {"wget", "-qO", pictName, link, NULL};
      execv("/usr/bin/wget", argv);
    }

    fprintf(prog_log, "Picture %s successfully downloaded\n", pictName);
    fflush(prog_log);
    
    sleep(5); // dilakukan tiap 5 detik
  }
}
```
Fungsi berikutnya yang dipanggil oleh `DownZip()` yaitu fungsi `zip()`. Nama dari file `.zip` akan ditampung di dalam sebuah array yaitu `zipName[]`. Nantinya sumber nama zip ini diambil dari array `folName[]`. Perintah zip dijalankan dengan bantuan `execv` yang mengeksekusi argumen `zip`. Berikut implemantasinya;
```c
void zip(char *folName)
{
  char zipName[150];
  // membuat nama zip

  sprintf(zipName, "%s.zip", folName);

  // melakukan zip
  char *argv[] = {"zip", "-rmq", zipName, folName, NULL};
  execv("/usr/bin/zip", argv);
}

```

Setelah proses download gambar dan zip folder selesai, maka pada fungsi `helpSucipto()` akan kembali menuliskan pesan ke file `prog_log` seperti implementasi berikut;
```c
    fprintf(prog_log, "Created a zip file named %s.zip\n", folName);
    fflush(prog_log);
```

Perlu diperhatikan, bahwa setiap sesudah pemanggilan fungsi `createFolder()` dan fungsi `DownZip()`, ditambahkan kode untuk menunggu proses selesai. Baris kode untuk menunggu ini dijalankan dengan bantuan `while-loop` dan fungsi `wait()` yang berargumen `w`. Variabel `w` adalah variabel bertipe `int` biasa. Berikut implemantasinya;
```c
int w;
.
.
.
while((wait(&w)) > 0);
```
Keseluruhan proses pembuatan nama folder, download gambar, zip folder download, penulisan pesan ke dalam file `prog_log` dilakukan di dalam sebuah blok `while-loop` dengan argumen 1 dan dijeda tiap 30 detik dengan bantuan perintah `sleep()`.
```c
while(1)
{
    //proses keseluruhan
    .
    .
    .
    sleep(30); //dilakukan tiap 30 detik
}
```

Berikut implementasi lengkap dari fungsi `helpSucipto()`
```c
void helpSucipto()
{
  time_t tnow;
  char folName[80];

  while(1)
  {
    int w;

    time(&tnow);
    strftime(folName, 80, "%Y-%m-%d_%X", localtime(&tnow));

    // call fungsi createFolder
    createFolder(folName);

    //menunggu pembuatan folder oleh createFolder
    while((wait(&w)) > 0);

    // call fungsi DownZip
    DownZip(folName);

    //menunggu proses download dan zip serta hapus direktorinya
    while((wait(&w)) > 0);

    fprintf(prog_log, "Created a zip file named %s.zip\n", folName);
    fflush(prog_log);
    
    sleep(30); //dilakukan tiap 30 detik 
  }
}
```

Berikutnya, untuk membuat `killer` dari program tersebut, diperlukan sebuah file `killer.sh`. File ini nantinya akan melakukan penghentian proses berdasarkan argumen mode yang diberikan saat program dijalankan. Pembuatan dan pengeksekusian file `killer.sh` ini dijalankan melalui sebuah fungsi bernama `assassin()`.

Pertama, terlebih dahulu dibuat sebuah file `killer.sh` dengan inisialisasi `pointer` bertipe `FILE` bernama `killgear`. File ini dibuat dengan mode `w` agar dapat melakukan penulisan script shell ke dalamnya. Berikut implemantasinya;
```c
FILE *killgear = fopen("killer.sh", "w");
```

Berikutnya dilakukan pemeriksaan terhadap argumen mode dengan bantuan fungsi `strcmp()`. Jika argumen yang diberikan adalah `-a`, maka `MODE_A` yang akan dijalankan, begitu pula jika argumennya adalah `-b`, maka `MODE_B` yang akan dijalankan. Fungsi `strcmp()` akan mengembalikan nilai `0` ketika hasil perbandingannya sama. Oleh karena itu, dapat diberikan tanda `!` untuk menegasikan hasilnya sehingga menjadi `True` dan blok `if` dapat dijalankan. Berikut implemantasinya;
```c
  if(!strcmp(argv[1], "-a") && argc == 2)
  {
    // menjalankan MODE_A : Menghentikan secara paksa, tanpa menunggu
    fprintf(killgear, "#!/bin/bash\nkillall -9 lukisan\nrm \"$0\"");
  }
  else if(!strcmp(argv[1], "-b") && argc == 2)
  {
    // menjalankan MODE_B : Menghentikan tapi menunggu proses selesai
    fprintf(killgear, "#!/bin/bash\nkill %d\nrm \"$0\"", pid);
  }
```

Pada implemantasi di atas, terdapat pengecekan isi dari array `argv[]` yang mana array ini berisikan argumen `-a` atau `-b`. Lalu terdapat array `argc[]` yang berfungsi untuk memastikan bahwa argumen eksekusi program hanya berjumlah 2, yaitu nama file script C ini yaitu `lukisan.c` dan argumen mode-nya. Jika mode yang dipilih adalah `MODE_A`, maka ke dalam file `killer.sh` dituliskan pesan sebagai berikut;
```sh
#!/bin/bash
killall -9 lukisan
rm $0
```
Pesan ini akan melakukan penghentian seluruh proses yang sedang berjalan pada file `lukisan.c` dengan perintah `killall -9 lukisan` (yang mana "`lukisan`" ini merupakan nama hasil compile file `lukisan.c`) lalu melakukan penghapusan dirinya dengan perintah `rm $0`.

Adapun untuk `MODE_B`, pesan yang dituliskan ke dalam file `killer.sh` adalah sebagai berikut;
```sh
#!/bin/bash
kill %d
rm $0
```
Format specifier `%d` di atas akan nantinya berisi sebuah PID proses yang akan dihentikan. Nilainya diambil dari variabel `pid`. Variabel `pid` ini merupakan argumen fungsi `assassin()`yang di-passing dari `main()`. Perintah pada file `killer.sh` dijalankan dengan menggunakan perintah `bash`. Terakhir, file `killer.sh` ditutup dengan fungsi `fclose()`. Berikut implementasi fungsi `assassin()`;
```c
void assassin(int argc, char **argv, int pid)
{
  int w;
  FILE *killgear = fopen("killer.sh", "w");

  if(!strcmp(argv[1], "-a") && argc == 2)
  {
    // menjalankan MODE_A : Menghentikan secara paksa, tanpa menunggu
    fprintf(killgear, "#!/bin/bash\nkillall -9 lukisan\nrm \"$0\"");
  }
  else if(!strcmp(argv[1], "-b") && argc == 2)
  {
    // menjalankan MODE_B : Menghentikan tapi menunggu proses selesai
    fprintf(killgear, "#!/bin/bash\nkill %d\nrm \"$0\"", pid);
  }
  
  fclose(killgear);
  
  // child proses agar killer.sh executable
  if(fork() == 0)
  {
    char *argv[] = {"chmod", "+x", "killer.sh", NULL};
    execv("/bin/chmod", argv);
  }

  if(!strcmp(argv[1], "-a"))
  {  
    if(fork() == 0)
    {
      char *killa[] = {"bash","killer.sh", NULL};
      execv("/bin/bash", killa);
      exit(0);
    }
  }
}
```
Berikut output jika dijalankan dengan MODE_A

![Output_Mode_A](/uploads/0650e58017e5849c62474cb20164565f/Output_Mode_A.png)

Berikut output jika dijalankan dengan MODE_B

![Output_Mode_B](/uploads/ad2328cf0d3ea50e8163cd8634a1fc1e/Output_Mode_B.png)

### Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United.

**a.** Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

**b.** Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

**c.** Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

**d.** Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

**Penyelesaian**

**a.** Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

Untuk melakukan yang diminta soal, maka pertama adalah melakukan download. Berikut merupakan fungsi download yang digunakan :
```c
void download(char file[], char url[]) {
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"wget",
                        "-O",  
                        file,  
                        url,   
                        NULL};
        execvp("wget", argv);
        exit(0);
    } else if (pid < 0) {
        printf("Fork failed.\n");
    } else {
        int status;
        waitpid(pid, &status, 0);
    }
}
```

Kemudian, untuk melakukan extract .zip tersebut dan menghapus file .zipnya, menggunakan dua fungsi berikut
```c
void unzip(char file[]){
    pid_t pid = fork();
    if (pid == 0){
        char *argv[] = {"unzip", file, "-d", "myDir", NULL};
        execvp("unzip", argv);
        exit(0);
    }
    else {
        int status;
        waitpid(pid, &status, 0);
    }
}
```
```c
void removeMe(char file[]){
    pid_t pid;
    pid = fork();
    if(pid == 0){
        char *arg[] = {"rm", file, NULL};
        execvp("rm", arg);
    }else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    }
    else {
        perror("Fork failed");
    }
}
```
![image](/uploads/2172b8f07ad561e93b08c9541ddf3ffc/image.png)

**b.** Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

Untuk menyelesaikan masalah ini, akan dilakukan sebuah method seperti directory listing. setiap kali menemukan file pada directory tersebut, akan dilakukan proses penghapusan file tersebut.
```c
void listAndRemove(){
    DIR *dp;
    struct dirent *ep;
    char path[] = "./myDir/players/";

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, "ManUtd")){
                continue;
            }

            char removeD[512];
            sprintf(removeD, "%s/%s", path, ep->d_name); 
            removeMe(removeD);
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}
```

Untuk definisi fungsi removeMe() adalah sebagai berikut
```c
void removeMe(char file[]){
    pid_t pid;
    pid = fork();
    if(pid == 0){
        char *arg[] = {"rm", file, NULL};
        execvp("rm", arg);
    }else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    }
    else {
        perror("Fork failed");
    }
}
```

**Sebelum running program**
![image](/uploads/237fb127443ca2c57b5c70c248a30aea/image.png)

**Setelah running program**
![image](/uploads/d2f9532cb9c2bfbe0e56763a5cf4ca53/image.png)

**c.** Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

Sebelum melakukan pemindahan file ke direktori berdasarkan posisi masing masing pemain, perlu dibuat untuk direktorinya itu sendiri.
```c
void createDir(char *foldername){
    pid_t pid = fork();

    if(pid==0){
        char *arg[] = {"mkdir", foldername, NULL};
        execvp("/bin/mkdir", arg);
        exit(0); 
    }
    else{
        wait(NULL); 
    }
}

void createDirAll(){
    createDir("Kiper");
    createDir("Bek");
    createDir("Gelandang");
    createDir("Penyerang");
}
```

Setelah menjalankan kode di atas, lakukan sebuah fungsi yang memindahkan ke direktori yang sesuai.
```c
void mainMove(char *pos){
    char path[] = "./myDir/players/";
    char destPath[256];

    snprintf(destPath, sizeof(destPath), "./%s", pos);

    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, pos)){
                moveFile(ep->d_name, path, destPath);
            }
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}
```

dimana untuk definisi fungsi moveFile ada pada fungsi berikut :
```c
void moveFile(char *filename, char *src, char *dest){
    pid_t pid = fork();
    if(pid==0){
        char srcfix[256];
        snprintf(srcfix, sizeof(srcfix), "%s/%s", src, filename);  

        char destfix[256];
        snprintf(destfix, sizeof(destfix), "%s/%s", dest, filename); 

        char *cp_arg[] = {"cp", srcfix, destfix, NULL};
        execvp("/bin/cp", cp_arg);

        char *rm_arg[] = {"rm", srcfix, NULL};
        execvp("/bin/rm", rm_arg);

        exit(0); 
    }
    else{
        wait(NULL);
    }
}
```

Untuk penggunaannya, dapat dilihat pada fungsi solve() berikut:
```c
void solve(){
    pid_t pid1, pid2, pid3;
    pid1 = fork();
    if(pid1 == 0){
        mainMove("Kiper");
        exit(0);
    }
    else{
        pid2 = fork();
        if(pid2 == 0){
            mainMove("Bek");
            exit(0);
        }
        else{
            pid3 = fork();
            if(pid3==0){
                mainMove("Gelandang");
                exit(0);
            }
            else{
                mainMove("Penyerang");
            }
        }
    }
}
```

Berikut merupakan hasilnya :
![image](/uploads/7ada44971f68b3a18dc6aa7d7390eeb7/image.png)
![image](/uploads/28b9e547c47f6a350ca49535b221283b/image.png)

**d.** Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Untuk penyelesaian ini, diharuskan untuk membuat fungsi buatTim(int a, int b, int c). dimana a, b, dan c adalah jumlah posisi, dimana a adalah pemain belakang, b gelandang, dan c adalah penyerang. Serta, ditambah satu dari posisi kiper.

Namun sebelum masuk ke fungsi tersebut, ada beberapa fungsi tambahan yang di implementasikan sehingga fungsi buatTim() dapat berjalan dengan semestinya.

Pertama, adalah fungsi getHighestRatedFile(). Fungsi ini akan mendapatkan nama file dengan rating terbaik dari suatu direktori, kemudian menghapus file tersebut di dalam directory. Nama file tersebut akan dikembalikan sebagai return value.
```c
char *getHighestRatedFile(char *foldername) {
    DIR *dp;
    struct dirent *ep;
    char *highest_rated_file = NULL;
    int highest_rating = -1;
    char filepath[512];
    char filename[256];
    char name[256];
    char team[256];
    char position[256];
    int rating;

    dp = opendir(foldername);
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (sscanf(ep->d_name, "%255[^_]_%255[^_]_%255[^_]_%d.png", name, team, position, &rating) != 4) {
                continue;
            }

            if (rating > highest_rating) {
                highest_rating = rating;
                sprintf(filepath, "%s/%s", foldername, ep->d_name);
                strcpy(filename, ep->d_name);
            }
        }
        (void)closedir(dp);

        highest_rated_file = (char*)malloc(strlen(filename) + 1);
        strcpy(highest_rated_file, filename);
        removeMe(filepath); 
    }
    else {
        perror("Couldn't open the directory");
    }

    return highest_rated_file;
}
```

Kedua, adalah fungsi untuk memindahkan file .txt nya ke /home/user
```c
void moveMe(char file[], char dest[]){
    pid_t pid;
    pid = fork();
    if(pid == 0){
        char *arg[] = {"mv", file, dest, NULL};
        execvp("mv", arg);
    }
}
```

Berikut merupakan fungsi utama dari penyelesaian soal bagian d ini.
```c
void buatTim(int b, int m, int f){
    char textFileName[256];
    sprintf(textFileName, "Formasi_%d-%d-%d.txt", b, m, f);
    
    FILE *fp = fopen(textFileName, "w");

    char *player = NULL;
    player = getHighestRatedFile("./Kiper");
    fwrite(player, strlen(player), 1, fp);

    fputc('\n', fp);

    while(b--){
        player = getHighestRatedFile("./Bek");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    while (m--)
    {
        player = getHighestRatedFile("./Gelandang");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    while (f--){
        player = getHighestRatedFile("./Penyerang");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    fclose(fp);

    moveMe(textFileName, "/home/{user}");
}
```

Pertama, fungsi ini akan mencari kiper terbaik, dilanjut b jumlah pemain terbaik dengan posisi bek, kemudian gelandang, dan terakhir penyerang.

Untuk penggunaan fungsi-fungsi diatas, dapat dilakukan dengan berikut:
```c
void solveD(int b, int m, int f){
    buatTim(b,m,f);
    solve();
}
```

fungsi solve() digunakan untuk mengembalikan "state" folder dari sebelum dilakukan proses buatTim(). Hal ini dikarenakan setiap kali pengambilan pemain terbaik, file akan dihapus. 

Output: buatTim(4,4,3)
![image](/uploads/fbb5dad3eac137e012281361f6df5d44/image.png)

Output: buatTim(4,5,1)
![image](/uploads/4610f1a67610f93b3420910e7d2d41cf/image.png)

### Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **Program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya. 

**Ketentuan :**

**a.**	Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

**b.**	Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa `Jam (0-23)`, `Menit (0-59)`, `Detik (0-59)`, Tanda `asterisk [ * ]` (value bebas), serta `path file .sh`.

**Penyelesaiaan :**

**1.** Fungsi untuk mengecek jumlah argumen yang dimasukkan sudah sesuai atau belum, jika tidak maka akan memanggil fungsi `print_error` dengan mencetak message  `"Wrong number of arguments"`
```sh
int main(int argc, char** argv) {
    if (argc != 5) {
        print_error("Wrong number of arguments");
    }
```
**2.** Mendeklarasikan variable `hour`, `minute` dan `second` untuk mendapatkan waktu yang masing-masing akan menyimpan nilai jam, menit, dan detik yang akan digunakan dalam program. Selanjutnya terdapat variabel `path` bertipe `char*` akan digunakan untuk menyimpan alamat memori dari sebuah string yang akan diproses dalam program.
```sh
    // Parsing argument
    int hour, minute, second;
    char* path;
```
**3.** Menetapkan `jam (0-23)` dan dapat menerima argumen arterisk `(*)`. Membuat blok `if-else` untuk memeriksa dan mengolah nilai argumen input pada indeks pertama `(argv[1])`. Jika nilai tersebut sama dengan karakter bintang `(*)`, maka program akan menetapkan nilai `-1` pada variabel hour. Namun jika nilai tersebut bukan bintang `(*)`, maka program akan melakukan konversi string ke integer dan melakukan pengecekan apakah nilai integer yang dihasilkan berada dalam rentang `0-23`. Jika nilai hour `< 0` atau `> 23`, maka program akan memanggil fungsi `print_error` dan mencetak message `"Invalid hour"`.
```sh
    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            print_error("Invalid hour");
        }
    }
```
**4.** Menetapkan argumen `menit (0-59)` sebagai indeks kedua `(argv[2])`. Nantinya melakukan konversi nilai argumen input pada indeks kedua menjadi tipe data integer dengan menggunakan fungsi `atoi`, dan menyimpan hasil konversi tersebut pada variabel `minute`. Lalu akan melakukanpengecekan terhadap nilai variabel `minute`. Jika nilai minute `< 0` atau `< 59`, maka program akan memanggil fungsi `print_error` dan mencetak message `"Invalid minute"`.
```sh
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59) {
        print_error("Invalid minute");
    }
```
**5.** Menetapkan argumen detik  `(0-59)` sebagai indeks ketiga `(argv[3])`. Nantinya melakukan konversi nilai argumen input pada indeks kedua menjadi tipe data integer dengan menggunakan fungsi `atoi`, dan menyimpan hasil konversi tersebut pada variabel `second`. Lalu akan melakukan pengecekan terhadap nilai variabel `Second`. Jika nilai minute `< 0` atau `< 59`, maka program akan memanggil fungsi `print_error` dan mencetak message `"Invalid Second"`.
```sh
    second = atoi(argv[3]);
    if (second < 0 || second > 59) {
        print_error("Invalid second");
    }
```
**6.** Menetapkan argumen `path` sebagai indeks keempat `(argv[4])`. Melakukan pemanggilan fungsi `access` dengan parameter `path` dan `F_OK`. Fungsi `access` digunakan untuk memeriksa apakah file yang direferensikan oleh `path` ada di sistem. Parameter `F_OK` menunjukkan bahwa program hanya ingin memeriksa apakah file tersebut ada atau tidak. Jika nilai yang dikembalikan oleh fungsi `access` adalah `-1`, artinya file yang direferensikan oleh `path` tidak ada di sistem. Pada kondisi tersebut, program akan memanggil fungsi `print_error` dan mencetak message `"File does not exist"`.
```sh
    path = argv[4];
    if (access(path, F_OK) == -1) {
        print_error("File does not exist");
    }
```

**b.**	Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan `“error”` apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error

**Penyelesaian :**
Membuat Fungsi Print Error yang akan dipanggil jika kondisi tidak sesuai pada argumen yang diinginkan :
```sh
void print_error(char* message) {
    fprintf(stderr, "Error: %s\n", message);
    exit(1);
}
```
Penjelasan :
`print_error` akan menerima sebuah argumen bertipe `char*` dengan nama `message`. Fungsi ini bertujuan untuk mencetak pesan `error` ke `stderr` dan menghentikan program dengan memanggil `exit(1)`.

**c.** Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

**Penyelesaiaan :**
Menggunakan implemantasi `Daemon`:
Daemon adalah suatu program yang berjalan di `background` secara terus menerus tanpa adanya interaksi secara langsung dengan user yang sedang aktif.
```sh
    // Variabel untuk menyimpan PID
    pid_t pid, sid;
    // Menyimpan PID dari Child Process
    pid = fork(); 

    // Keluar saat fork gagal (nilai variabel pid < 0)
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    //Keluar saat fork berhasil (nilai variabel pid adalah PID dari child process)
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    //Membuat session baru dan menjadikan proses yang sedang berjalan sebagai session leader
    sid = setsid();
    if (sid < 0) { //sid kurang dari 0 (gagal membuat session baru)
        exit(EXIT_FAILURE);
    }
    
    //mengubah direktori kerja daemon menjadi direktori root (/).
    if ((chdir("/")) < 0) { 
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

```
**Menjalankan Program**

Command compile file C :
```sh
gcc mainan.c -o test
```
Contoh masukkan argumen :
```sh
./test \* 12 44 /home/bilaaripa/Banabil/programcron.sh
```
Check apakah sudah berjalan di `background` :
```sh
ps aux
```
bisa juga menggunakan :
```sh
ps -aux | grep "test"
```
Melakukan penghentian proses dengan `kill proses` :
```sh
kill -9 <PID>
```
**Output dengan argumen benar :**
![pr2-3](/uploads/091e009356647415fc81657d57dd6055/pr2-3.png)

**Output dengan argumen salah :**
Akan mengeluarkan pesan `error` :
![pr2-4](/uploads/12f9601b64708b5e8ff6faae398fb5b4/pr2-4.png)

**Kill Proses :**
![pr2-5](/uploads/ff0cc599fe53ae892493442b2855a9ea/pr2-5.png)
### Kendala
Kendala yang kami alami dalam pengerjaan soal shift modul 2 adalah yakni pemahaman tentang konsep fork dan execv. Namun dengan adanya modul dan sumber informasi digital akhirnya praktikum shift modul 2 dapat diselesaikan sesuai yang diharapkan.

