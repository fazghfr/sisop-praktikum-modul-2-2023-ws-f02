#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

void print_error(char* message) {
    fprintf(stderr, "Error: %s\n", message);
    exit(1);
}

int main(int argc, char** argv) {
    if (argc != 5) {
        print_error("Wrong number of arguments");
    }

    // Parsing argument
    int hour, minute, second;
    char* path;

    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            print_error("Invalid hour");
        }
    }

    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59) {
        print_error("Invalid minute");
    }

    second = atoi(argv[3]);
    if (second < 0 || second > 59) {
        print_error("Invalid second");
    }

    path = argv[4];
    if (access(path, F_OK) == -1) {
        print_error("File does not exist");
    }

    //Implemantasi Daemon sesuai modul
    pid_t pid, sid; // Variabel untuk menyimpan PID
    pid = fork(); // Menyimpan PID dari Child Process

    // Keluar saat fork gagal (nilai variabel pid < 0)
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    //Keluar saat fork berhasil (nilai variabel pid adalah PID dari child process)
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    //Membuat session baru dan menjadikan proses yang sedang berjalan sebagai session leader
    sid = setsid();
    if (sid < 0) { //sid kurang dari 0 (gagal membuat session baru)
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) { //mengubah direktori kerja daemon menjadi direktori root (/). chdir() akan mengembalikan nilai kurang dari 0 jika gagal dan berhasil mengembalikan nilai 0.
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Main loop
    while (1) {
        time_t current_time = time(NULL);
        struct tm* local_time = localtime(&current_time);
        int current_hour = local_time->tm_hour; //mendeklarasikan variabel current_hour sebagai int dan memberinya nilai jam saat ini yang diperoleh dari local_time
        int current_minute = local_time->tm_min; //mendeklarasikan variabel current_minute sebagai int dan memberinya nilai jam saat ini yang diperoleh dari local_time
        int current_second = local_time->tm_sec; //mendeklarasikan variabel current_secondn sebagai int dan memberinya nilai jam saat ini yang diperoleh dari local_time

        //pengecekan apakah jam (hour), menit (minute), dan detik (second) saat ini sama dengan parameter yang diberikan saat menjalankan program atau tidak.
        if ((hour == -1 || hour == current_hour) && minute == current_minute && second == current_second) {
            //Jika sama, maka program akan menjalankan sebuah script Bash dengan menggunakan fungsi execv
            char *args[] = {"bash", argv[4], NULL};
            execv("/bin/bash", args);
            print_error("Failed to execute script");
        }

        //program akan beristirahat selama satu detik menggunakan fungsi sleep(1) sebelum melakukan pengecekan waktu lagi pada iterasi berikutnya
        sleep(1);
    }

    return 0;
}

