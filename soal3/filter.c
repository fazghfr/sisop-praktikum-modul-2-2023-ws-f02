#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

void download(char file[], char url[]) {
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"wget",
                        "-O",  
                        file,  
                        url,   
                        NULL};
        execvp("wget", argv);
        exit(0);
    } else if (pid < 0) {
        printf("Fork failed.\n");
    } else {
        int status;
        waitpid(pid, &status, 0);
    }
}

void unzip(char file[]){
    pid_t pid = fork();
    if (pid == 0){
        char *argv[] = {"unzip", file, "-d", "myDir", NULL};
        execvp("unzip", argv);
        exit(0);
    }
    else {
        int status;
        waitpid(pid, &status, 0);
    }
}

void removeMe(char file[]){
    pid_t pid;
    pid = fork();
    if(pid == 0){
        char *arg[] = {"rm", file, NULL};
        execvp("rm", arg);
    }else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
    }
    else {
        perror("Fork failed");
    }
}


void listAndRemove(){
    DIR *dp;
    struct dirent *ep;
    char path[] = "./myDir/players/";

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, "ManUtd")){
                continue;
            }

            char removeD[512];
            sprintf(removeD, "%s/%s", path, ep->d_name); 
            removeMe(removeD);
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}

void removeZip(char file[]){
    pid_t pid = fork();
    if(pid == 0){
        char *argv[] = {"rm", file, NULL};
        execvp("rm", argv);
        exit(0);
    }
    else {
        int status;
        waitpid(pid, &status, 0);
    }
}

void createDir(char *foldername){
    pid_t pid = fork();

    if(pid==0){
        char *arg[] = {"mkdir", foldername, NULL};
        execvp("/bin/mkdir", arg);
        exit(0); 
    }
    else{
        wait(NULL); 
    }
}

void createDirAll(){
    createDir("Kiper");
    createDir("Bek");
    createDir("Gelandang");
    createDir("Penyerang");
}

void moveFile(char *filename, char *src, char *dest){
    pid_t pid = fork();
    if(pid==0){
        char srcfix[256];
        snprintf(srcfix, sizeof(srcfix), "%s/%s", src, filename);  

        char destfix[256];
        snprintf(destfix, sizeof(destfix), "%s/%s", dest, filename); 

        char *cp_arg[] = {"cp", srcfix, destfix, NULL};
        execvp("/bin/cp", cp_arg);

        char *rm_arg[] = {"rm", srcfix, NULL};
        execvp("/bin/rm", rm_arg);

        exit(0); 
    }
    else{
        wait(NULL);
    }
}


void mainMove(char *pos){
    char path[] = "./myDir/players/";
    char destPath[256];

    snprintf(destPath, sizeof(destPath), "./%s", pos);

    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, pos)){
                moveFile(ep->d_name, path, destPath);
            }
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}


void solve(){
    pid_t pid1, pid2, pid3;
    pid1 = fork();
    if(pid1 == 0){
        mainMove("Kiper");
        exit(0);
    }
    else{
        pid2 = fork();
        if(pid2 == 0){
            mainMove("Bek");
            exit(0);
        }
        else{
            pid3 = fork();
            if(pid3==0){
                mainMove("Gelandang");
                exit(0);
            }
            else{
                mainMove("Penyerang");
            }
        }
    }
}

char *getHighestRatedFile(char *foldername) {
    DIR *dp;
    struct dirent *ep;
    char *highest_rated_file = NULL;
    int highest_rating = -1;
    char filepath[512];
    char filename[256];
    char name[256];
    char team[256];
    char position[256];
    int rating;

    dp = opendir(foldername);
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (sscanf(ep->d_name, "%255[^_]_%255[^_]_%255[^_]_%d.png", name, team, position, &rating) != 4) {
                continue;
            }

            if (rating > highest_rating) {
                highest_rating = rating;
                sprintf(filepath, "%s/%s", foldername, ep->d_name);
                strcpy(filename, ep->d_name);
            }
        }
        (void)closedir(dp);

        highest_rated_file = (char*)malloc(strlen(filename) + 1);
        strcpy(highest_rated_file, filename);
        removeMe(filepath); 
    }
    else {
        perror("Couldn't open the directory");
    }

    return highest_rated_file;
}


void moveMe(char file[], char dest[]){
    pid_t pid;
    pid = fork();
    if(pid == 0){
        char *arg[] = {"mv", file, dest, NULL};
        execvp("mv", arg);
    }
}

void buatTim(int b, int m, int f){
    char textFileName[256];
    sprintf(textFileName, "Formasi_%d-%d-%d.txt", b, m, f);
    
    FILE *fp = fopen(textFileName, "w");

    char *player = NULL;
    player = getHighestRatedFile("./Kiper");
    fwrite(player, strlen(player), 1, fp);

    fputc('\n', fp);

    while(b--){
        player = getHighestRatedFile("./Bek");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    while (m--)
    {
        player = getHighestRatedFile("./Gelandang");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    while (f--){
        player = getHighestRatedFile("./Penyerang");
        fwrite(player, strlen(player), 1, fp);

        fputc('\n', fp);
    }

    fclose(fp);

    moveMe(textFileName, "/home/{user}");
}

void solveA(){
    download("download.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download");
    unzip("download.zip");
    removeZip("download.zip");
}

void solveB(){
    listAndRemove();
}

void solveC(){
    createDirAll();
    solve();
}

void solveD(int b, int m, int f){
    buatTim(b,m,f);

    solve();
}

int main() {
    int status;
    pid_t pid;

    // Start solveA
    pid = fork();
    if (pid == 0) {
        solveA();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveB
    pid = fork();
    if (pid == 0) {
        solveB();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveC
    pid = fork();
    if (pid == 0) {
        solveC();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveD
    pid = fork();
    if (pid == 0) {
        solveD(4,5,1);
        exit(0);
    }
    else {
        wait(&status);
    }

    return 0;
}