#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>
#include <dirent.h>

void download(char file[], char url[]) {
    int status;
    pid_t pid = fork();

    if(pid==0){
        char *argv[] = {"wget",
                        "-O",  // output file
                        file,  // specified output file name
                        url,   // URL of the file to download
                        NULL};

        execvp("wget", argv);
    } 
    else if (pid < 0){
        printf("fork error");
        exit(1);
    }

    else {
        wait(&status);
    }

}

void unzip(char file[]){
    pid_t pid = fork();

    if(pid == 0){
        char *arg[] = {"unzip", "-q", file, "-d", "downloaded", NULL};
        execvp("unzip", arg);
    } else if (pid < 0){
        printf("fork error");
        exit(1);
    }
}

void solveA(){
    char filename[] = "animals.zip";
	download(filename, "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq");
    unzip(filename);
}


int getFileNumAtDir(char path[]){
    DIR *dp = opendir(path);
    struct dirent *ep;
    int count = 0;
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
           if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0){
                continue;
           }
           count++;
      }

      (void) closedir (dp);
    } else {
        perror ("Couldn't open the directory");
        return - 1;
    }

    return count;
}


int getRandNumber(char folderName[]){
    srand(time(NULL));
    return rand()%getFileNumAtDir(folderName);
}

char* chooseRandom(char foldername[]){
    int r = getRandNumber(foldername);
    int count = 0;
    DIR *dp = opendir(foldername);
    struct dirent *ep;

    char *returnVal = NULL;
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
           if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0){
                continue;
           }

           if(count == r){
                returnVal = (char*)malloc(strlen(ep->d_name)+2);
                returnVal = (char*)ep->d_name;
                break;
           }
           count++;
      }

      (void) closedir (dp);
    } else {
        perror ("Couldn't open the directory");
        return NULL;
    }

    return returnVal;
}

void solveB(){
    printf("%s", chooseRandom("./downloaded"));
}

void createDir(char *foldername){
    pid_t pid = fork();

    if(pid==0){
        char *arg[] = {"mkdir", foldername, NULL};
        execvp("/bin/mkdir", arg);
        exit(0); 
    }
    else{
        wait(NULL); 
    }
}

void createDirAll(){
    createDir("HewanDarat");
    createDir("HewanAir");
    createDir("HewanAmphibi");
}

void moveFile(char *filename, char *src, char *dest){
    pid_t pid = fork();
    if(pid==0){
        char srcfix[256];
        snprintf(srcfix, sizeof(srcfix), "%s/%s", src, filename);  

        char destfix[256];
        snprintf(destfix, sizeof(destfix), "%s/%s", dest, filename); 

        char *mv_arg[] = {"mv", srcfix, destfix, NULL};
        execvp("/bin/mv", mv_arg);

        exit(0); 
    }
    else{
        wait(NULL);
    }
}

void mainMove(char *det){
    char path[] = "downloaded";
    

    DIR *dp;
    struct dirent *ep;
    dp = opendir(path);
    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) {
                continue;
            }

            else if(strstr(ep->d_name, det)){
                char destPath[256];

                if(strstr(ep->d_name, "air")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Air");
                    moveFile(ep->d_name, path, destPath);
                }

                else if(strstr(ep->d_name, "darat")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Darat");
                    moveFile(ep->d_name, path, destPath);
                }

                else if(strstr(ep->d_name, "amphibi")){
                    snprintf(destPath, sizeof(destPath), "./Hewan%s", "Amphibi");
                    moveFile(ep->d_name, path, destPath);
                }
                
            }
        }
      (void) closedir (dp);
    } 
    else perror ("Couldn't open the directory");
}

void solveC(){
    createDirAll();
    mainMove("air");
    mainMove("darat");
    mainMove("amphibi");
}

void removeMe(char *foldername) {
    int status;
    pid_t pid = fork();

    if(pid == 0){
        char *rm_args[] = {"rm", "-rf", foldername, NULL};
        execvp("rm", rm_args);
    } else {
        wait(&status);
    }
}


void zipDirAndContents(char folderName[]){
    int status;
    pid_t pid = fork();
    
    char *zipfile = NULL;
    zipfile = (char*)malloc(strlen(folderName)+5);
    strcpy(zipfile, folderName);
    strcat(zipfile, ".zip");
    if(pid == 0){
        char *zip_args[] = {"zip", "-r", zipfile, folderName, NULL};
        execvp("zip", zip_args);
    } else {
        wait(&status);
        removeMe(folderName);
    }
    free(zipfile);
}

void solveD(){
    zipDirAndContents("HewanAir");
    zipDirAndContents("HewanDarat");
    zipDirAndContents("HewanAmphibi");
}

int main(){
    int status;
    pid_t pid;

    // Start solveA
    pid = fork();
    if (pid == 0) {
        solveA();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveB
    pid = fork();
    if (pid == 0) {
        solveB();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveC
    pid = fork();
    if (pid == 0) {
        solveC();
        exit(0);
    }
    else {
        wait(&status);
    }

    // Start solveD
    pid = fork();
    if (pid == 0) {
        solveD();
        exit(0);
    }
    else {
        wait(&status);
    }
}
	