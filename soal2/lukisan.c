#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>


FILE *prog_log;


void download(char *folName)
{
  time_t tnow;
  int i, size;

  char pictName[201], buffer[200], link[200];

  for(i=0; i < 15; i++) // 15 gambar
  {
    time(&tnow);
    strftime(buffer, 200, "%Y-%m-%d_%X", localtime(&tnow));

    sprintf(pictName, "%s/%s", folName, buffer); // nama file gambar yang di-download

    // mendapatkan Epoch Unix dan mengatur size download 
    size = (int)time(NULL); 				
    size = (size % 1000) + 50;

    sprintf(link, "https://picsum.photos/%d", size);

    if(fork() == 0)
    {
      char *argv[] = {"wget", "-qO", pictName, link, NULL};
      execv("/usr/bin/wget", argv);
    }

    fprintf(prog_log, "Picture %s successfully downloaded\n", pictName);
    fflush(prog_log);

    sleep(5); // dilakukan tiap 5 detik
  }
}


void zip(char *folName)
{
  char zipName[150];

  // membuat nama zip
  sprintf(zipName, "%s.zip", folName);

  // melakukan zip
  char *argv[] = {"zip", "-rmq", zipName, folName, NULL};
  execv("/usr/bin/zip", argv);
}


void DownZip(char *folName)
{
  if(fork() == 0)
  {
    fprintf(prog_log, "Created a folder named %s\n", folName);
    fflush(prog_log);

    // fungsi untuk melakukan download
    download(folName);

    //fungsi untuk melakukan zip
    zip(folName);
  }
}


void createFolder(char *folName)
{
  if(fork() == 0)
  {
    char *argv[] = {"mkdir", "-p", folName, NULL};
    execv("/bin/mkdir", argv);
  }
}



void helpSucipto()
{
  time_t tnow;
  char folName[80];

  while(1)
  {
    int w;
    time(&tnow);

    strftime(folName, 80, "%Y-%m-%d_%X", localtime(&tnow));

    // call fungsi createFolder
    createFolder(folName);

    //menunggu pembuatan folder oleh createFolder
    while((wait(&w)) > 0);

    // call fungsi DownZip
    DownZip(folName);

    //menunggu proses download dan zip serta hapus direktorinya
    while((wait(&w)) > 0);


    fprintf(prog_log, "Created a zip file named %s.zip\n", folName);
    fflush(prog_log);

    sleep(30); //dilakukan tiap 30 detik 
  }
}


void assassin(int argc, char **argv, int pid)
{
  int w;
  FILE *killgear = fopen("killer.sh", "w");

  if(!strcmp(argv[1], "-a") && argc == 2)
  {
    // menjalankan MODE_A : Menghentikan secara paksa, tanpa menunggu
    fprintf(killgear, "#!/bin/bash\nkillall -9 lukisan\nrm \"$0\"");
  }
  else if(!strcmp(argv[1], "-b") && argc == 2)
  {
    // menjalankan MODE_B : Menghentikan tapi menunggu proses selesai
    fprintf(killgear, "#!/bin/bash\nkill %d\nrm \"$0\"", pid);
  }

  fclose(killgear);

  // child proses agar killer.sh executable
  if(fork() == 0)
  {
    char *argv[] = {"chmod", "+x", "killer.sh", NULL};
    execv("/bin/chmod", argv);
  }
  
  if(!strcmp(argv[1], "-a"))
  {  
    if(fork() == 0)
    {
      char *killa[] = {"bash","killer.sh", NULL};
      execv("/bin/bash", killa);
      exit(0);
    }
  }

}


int main(int argc, char**argv)
{
  pid_t pid, sid;
  char folName[80];
  time_t tnow;

  pid = fork();
  if(pid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if(pid > 0)
  {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if(sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  //membuka file log_prog.txt dengan mode write dan read
  //prog_log dibuat hanya untuk tracking proses
  prog_log = fopen("log_prog.txt", "w+");

  //buat fungsi killer bernama assassin
  assassin(argc, argv, (int)getpid());

  fprintf(prog_log,"Program Assassin exists\n");
  fflush(prog_log);

  //program membantu Sucipto
  helpSucipto(); 

  fclose(prog_log);

  return 0;
}
